<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vedita_orginal');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'g6tdwr9ossxq8dn8skvesogpcomupj2fzg6k6nlano9ynkctq4mtfetinyqhcwzx');
define('SECURE_AUTH_KEY',  '1llpsw2x5vrsmt8sf8dldsqkozjow8qdkbwlziweq9kwarmellrc5pdhtshzs9hr');
define('LOGGED_IN_KEY',    'e1zdn81jtqnwkkyzbwjymwkj6q7nveduuser8rk8dnkhtrzfiwd8djofkzglzdbb');
define('NONCE_KEY',        'l0wudmfwofhxo4cmmriwdgirzakemzlzwqmybtfcizhmd9jjppzkxzli4zbvjzdf');
define('AUTH_SALT',        'fw4jbt56sgf2n6zygkvgxv962umnfipd8jcbk9lcvm537jdyxlnkio3bvqieg3kl');
define('SECURE_AUTH_SALT', 'unoit4xkvggvv6n4py0cgbodvhnu7bjbbprlgrxbqrsm5borqid9elj0ixsxfhnm');
define('LOGGED_IN_SALT',   'qfgpaz1oht28ttjzje1dbufqqr3ufbnkoe3xfo9gih4dfcua3wjtgyu5zldptnqn');
define('NONCE_SALT',       'l4buev6wcvcunmmu1zzqfqfot9cwvdeiidk82ocutxatrkxrj057azgmvqx58hh6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'vv_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
